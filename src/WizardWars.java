import com.wizard.*;

import java.util.Scanner;

public class WizardWars {

    public static void main(String[] args) {
        CharacterCreation characterCreation = new CharacterCreation();
        System.out.println("Welcome to Wizard Wars. Lets create our character");
        Wizard createdWizard = new Wizard();
        Scanner scanner = new Scanner(System.in);
        while (createdWizard.getName() == null || createdWizard.getName().isEmpty()) {
            System.out.println("Please enter your characters' first name");
            String incomingFirstName = scanner.nextLine();
            if (characterCreation.validateName(incomingFirstName)) {
                createdWizard.setName(incomingFirstName);
            }

        }

        while (createdWizard.getSurname() == null || createdWizard.getSurname().isEmpty()) {
            System.out.println("Please enter your characters' surname");
            String incomingSurname = scanner.nextLine();
            if (characterCreation.validateSurname(incomingSurname)) {
                createdWizard.setSurname(incomingSurname);
            }

        }
        //We got the surname and the name inside the createdWizard Object
        System.out.println("Welcome " + createdWizard.getName() + " " +
                createdWizard.getSurname());


        WizardShop wizardShop = new WizardShop();
        System.out.println("Welcome to Wizard Shop. " +
                "Can I take your order? Write spell Name to buy.");
        wizardShop.printAllSpells();

        String incomingSpell = "";
        incomingSpell = scanner.nextLine();
        while (!incomingSpell.equalsIgnoreCase("done")) {
            //Checks and buys the spell for the wizard. Assigns to wizard
            //memory.
            wizardShop.buy(incomingSpell, createdWizard);
            System.out.println("Do you want to buy some more? " +
                    "Write \"done\" if you are finished.");
            incomingSpell = scanner.nextLine();
        }
        System.out.println(createdWizard.getListOfSpellsWizardKnows());

        //you will fight against this guy.
        Wizard opponent = new Wizard();
        Opponent opponentUtility = new Opponent();
        SpellUtilities spellUtilities = new SpellUtilities();

        System.out.println("Let the Duel Begin.");
        while (createdWizard.getHealth() > 0 && opponent.getHealth() > 0) {
            System.out.println("Player Health: " + createdWizard.getHealth()
                    + " Opponent Health: " + opponent.getHealth());
            int playerValue = 0;  //damage of healing information
            int opponentValue = 0;//damage or healing information of the opponent.

            System.out.println("Cast your Spell Player");
            incomingSpell = scanner.nextLine();
            if (createdWizard.getListOfSpellsWizardKnows().contains(incomingSpell)) {
                playerValue = spellUtilities.cast(incomingSpell);
            } else {
                System.out.println("You do not know this spell");
            }

            System.out.println("");
            System.out.println("Opponent is casting");
            opponentValue = opponentUtility.castDecision(createdWizard, opponent);

            if (playerValue > 0) {
                //healing
                int missingHealth = 100 - createdWizard.getHealth();
                if (playerValue > missingHealth) {
                    createdWizard.setHealth(100);
                } else {
                    createdWizard.setHealth(createdWizard.getHealth() + playerValue);
                }
            }
            if(opponentValue>0){
                //healing
                int missingHealth = 100 - opponent.getHealth();
                if(opponentValue>missingHealth){
                    opponent.setHealth(100);
                }else
                {
                    opponent.setHealth(opponent.getHealth()+opponentValue);
                }
            }
            if (playerValue < 0) {
                System.out.println("Damage caused:"+playerValue);
                opponent.setHealth(opponent.getHealth()+playerValue);
            }
            if (opponentValue < 0) {
                System.out.println("Damage caused:"+opponentValue);
                createdWizard.setHealth(createdWizard.getHealth()+opponentValue);
            }
        }

        //Duel is over
        System.out.println("The duel is over");
        if(opponent.getHealth()<=0 && createdWizard.getHealth()<=0){
            System.out.println("It is a draw");
        }else if(opponent.getHealth()<=0){
            System.out.println("You won");
        }else{
            System.out.println("You lost.");
        }




    }


}
