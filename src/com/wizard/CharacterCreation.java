package com.wizard;

public class CharacterCreation {

    public boolean validateName(String firstName){
        if(firstName==null || firstName.isEmpty()){
            System.out.println("First name is not suitable. You did not enter any name");
            return false;
        }
        if(firstName.contains(" ")){
            System.out.println("First name must not contain any space");
            return false;
        }

        String filteredName = eraseNonLetters(firstName);
        if(!firstName.equalsIgnoreCase(filteredName)){
            System.out.println("First name is not suitable. No Numbers/Punctuation.");
            return false;
        }
        return true;
    }

    public boolean validateSurname(String surName){
        if(surName==null || surName.isEmpty()){
            System.out.println("Surname is not suitable. You did not enter any name");
            return false;
        }
        String filteredName = eraseNonLetters(surName);
        if(!surName.equalsIgnoreCase(filteredName)){
            System.out.println("Surname is not suitable. No Numbers/Punctuation.");
            return false;
        }
        return true;
    }

    private String eraseNonLetters(String word) {
        String result = word.replaceAll("\\d","");
        result = result.replaceAll("\\p{Punct}","");
        return result;
    }


}
