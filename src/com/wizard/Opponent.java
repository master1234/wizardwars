package com.wizard;


import java.util.Random;

public class Opponent extends Wizard{

    /**
     *  Task1: If health of opponent below 35
     *          50% change to cast healing
     *          50% there will be below decision
     *                  he will get players health
     *                  if playerHealth is below 11 he will castFlame
     *                  else if playerHealth is below 17 he will castLightening
     *                  else if playerHealth is below 23 he will castWind
     *                  else if playerHealth is below 30 he will castFireball
     *                  else if playerHealth is below 37 he will castExplosion
     *                  else if playerHealth is below 75 he will castDeath
     *                  else castHealing
     *
     */

    /**
     *  Task2: If health of opponent bigger than the player
     *          20% change to castLightening
     *          20% to castFireball
     *          20% to castWind
     *          20% to castFlmae
     *          20% to cast Explosion
     *
     */
    /**
     *  Task3: rest of the conditions
     *          15% change to castLightening
     *          10% to castFireball
     *          15% to castWind
     *          20% to castFlmae
     *          20% to cast Explosion
     *          20% to cast healing
     */
    public int castDecision(Wizard player,Wizard opponent){
        /**
         * Task1:
         */
        SpellUtilities spellUtilities = new SpellUtilities();
        Random random = new Random();
        if(opponent.getHealth()<35){
            int decisionRate = random.nextInt(100);
            if(decisionRate<50){
                return  spellUtilities.castHealing();
            }else{
                int playerHealth = player.getHealth();

                if(playerHealth<11){
                    return spellUtilities.castFlame();
                }else if(playerHealth<17){
                    return spellUtilities.castLightening();
                }else if (playerHealth<23){
                    return spellUtilities.castWind();
                }else if (playerHealth<30){
                    return spellUtilities.castFireball();
                }else if(playerHealth<37){
                    return spellUtilities.castExplosion();
                }else if(playerHealth<75){
                    return spellUtilities.castDeath();
                }else{
                    return  spellUtilities.castHealing();
                }
            }
        }else if(opponent.getHealth()>player.getHealth()){
            /**
             * Task2:
             */
            int decisionRate = random.nextInt(100);
            if(decisionRate<20){
                return spellUtilities.castLightening();
            }else if (decisionRate<40){
                return spellUtilities.castFireball();
            } else if (decisionRate<60){
                return spellUtilities.castWind();
            }else if(decisionRate<80){
                return spellUtilities.castFlame();
            }else{
                return spellUtilities.castExplosion();
            }
        }else{
            /**
             * Task3:
             * *          15% change to castLightening
             *      *          10% to castFireball
             *      *          15% to castWind
             *      *          20% to castFlmae
             *      *          20% to cast Explosion
             *      *          20% to cast healing
             */
            int decisionRate = random.nextInt(100);
            if(decisionRate<15){
                return spellUtilities.castLightening();
            }else  if(decisionRate<25){
                return spellUtilities.castFireball();
            }else  if(decisionRate<40){
                return spellUtilities.castWind();
            }else  if(decisionRate<60){
                return spellUtilities.castFlame();
            }else  if(decisionRate<80){
                return spellUtilities.castExplosion();
            }else{
                return spellUtilities.castHealing();
            }

        }









    }

}
