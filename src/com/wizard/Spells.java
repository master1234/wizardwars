package com.wizard;


import java.util.HashMap;

public class Spells {
    HashMap<String, Spell> spellHashMap = new HashMap<>();

    public Spells(){
        spellHashMap.put("Lacarnum Inflamarae",new Spell("Lacarnum Inflamarae",25,10,25,70));
        spellHashMap.put("Lumos Solem",new Spell("Lumos Solem",45,17,35,60));
        spellHashMap.put("Everte Statum",new Spell("Everte Statum",45,23,29,65));
        spellHashMap.put("Arania Exumai",new Spell("Arania Exumai",50,30,45,50));
        spellHashMap.put("Bombarda",new Spell("Bombarda",60,37,45,65));
        spellHashMap.put("Avada Kedavra",new Spell("Avada Kedavra",100,100,100,10));
        spellHashMap.put("Vulnera Sanentur",new Spell("Vulnera Sanentur",25,10,20,70));
    }

    public HashMap<String, Spell> getSpellHashMap() {
        return spellHashMap;
    }

    public void setSpellHashMap(HashMap<String, Spell> spellHashMap) {
        this.spellHashMap = spellHashMap;
    }
}
